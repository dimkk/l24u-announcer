import { sync } from "glob";
import { union } from "lodash";
import { IData } from "../customTypings/IData";


export default class Config {
	public static port : number = process.env.PORT || 8181;
	public static routes : string = "./src/routes/**/*.ts";
	public static models : string = "./src/models/**/*.ts";

	public static globFiles(location : string) : Array<string> {
		return union([], sync(location));
	}
	public static urlToBbTopicApi = process.env.NBB_TOPIC_WRITE_API || 'https://l24u.ru/api/v1/topics/';//'http://changes.l24u.ru';//
	public static urlToBbTopicReadApi = process.env.NBB_TOPIC_READ_API || 'https://l24u.ru/api/topic/';//'http://changes.l24u.ru';//
	public static token = process.env.NBB_TOKEN || '028ed5b8-7267-4d84-97a0-8276345552fd';


	public static data:IData[] = 
	[
		//ядро
		{ "catId":process.env.NBB_CAT_CORE_ID || 7, "guid":process.env.NBB_CAT_CORE_GUID || "4036d8b1-5d51-4cd6-858f-89ccb91c14cb" },
		//data
		{ "catId":process.env.NBB_CAT_DATA_ID, "guid":process.env.NBB_CAT_DATA_GUID },
		//geo
		{ "catId":process.env.NBB_CAT_GEO_ID, "guid":process.env.NBB_CAT_GEO_GUID },
		//dsetup
		{ "catId":process.env.NBB_CAT_DSETUP_ID, "guid":process.env.NBB_CAT_DSETUP_GUID },
		//patch
		{ "catId":process.env.NBB_CAT_PATCH_ID, "guid":process.env.NBB_CAT_PATCH_GUID },
		//updater
		{ "catId":process.env.NBB_CAT_UPDATER_ID, "guid":process.env.NBB_CAT_UPDATER_GUID },
	]
}