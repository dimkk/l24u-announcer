import * as express from "express";
import Config from "../config/config";
import postToBb from "../services/postToBb";

export default class IndexController {
    test:any = {'fs':'jhu'};
    message:string;
    branch:string;
    catId:string;
    postToBb= new postToBb();

    IndexController () {
    };

    read = (req: express.Request, res: express.Response, next: Function): void => {
        this.test = req.url.replace('/', '');
        console.log(this.test);
        let data = Config.data.filter((item)=>{
            return item.guid === this.test;
        });
        if (data.length > 0){
            this.test = data[0].catId;
            res.send(JSON.stringify(this.test));
        } else {
            res.send(404);
        }
        
    }
    post = (req: express.Request, res: express.Response, next: Function): void => {

        this.test = req.url.replace('/', '');
        let data = Config.data.filter((item)=>{
            return item.guid === this.test;
        });
        if (data.length > 0){
            this.catId = data[0].catId;
            try {
                this.message = req.body.push.changes[0].new.target.message;
                this.branch = req.body.push.changes[0].new.name;
            } catch(err) {
                console.log(err);
                res.send(500, err);
            }
            if (this.branch === 'master'){
                 this.postToBb.post(this.catId, this.message, this.branch).then((opts)=>{
                    this.postToBb.closeTopics(this.message, opts);
                })
            }
           
            
        } else {
            res.send(404);
        }

        
        res.send(JSON.stringify(this.test));
    }
}