import * as express from "express";
import IndexController from "../controllers/index.server.controller";
import { IData } from "../customTypings/IData";
import Config from "../config/config";

export default class IndexRoute {
	constructor(app : express.Express) {
		let ctrl = new IndexController();
		// app.route("/")
		// 	.get(ctrl.read);
		// app.route("/")
		// 	.post(ctrl.post);
	    Config.data.forEach((param) => {
			this.setRoute(app, param, ctrl);
		});
	}

	setRoute = (app : express.Express, params:IData, ctrl:any) => {
		app.route("/" + params.guid)
			.get(ctrl.read);
		app.route("/" + params.guid)
			.post(ctrl.post);
	}
}