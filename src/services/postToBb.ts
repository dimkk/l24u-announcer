import Config from "../config/config";
let request = require('request');
let querystring = require('querystring');
let q = require('q');
let _ = require('lodash');
var md = require('to-markdown');

var Entities = require('html-entities').XmlEntities;
 
let entities = new Entities();

request.defaults({jar: true});
export default class postToBb {
    private options = {
            headers: {
                    'Authorization': 'Bearer ' + Config.token
            }
    }
    postToBb () {
    };

    private tags:any;

    public post = (cId:string, message:string, branch:string) => {
        let def = q.defer();
        console.log(Config.urlToBbTopicApi);

        let strings = message.split('\n');
        let title = strings[0];
        strings.shift();

        

        message = strings.join('\n');
        let re = /\S*#(?:\[[^\]]+\]|\S+)/;
        let topicsIds = message.match(re);

        if (topicsIds){
            topicsIds.forEach((id)=>{
                message = message.replace(id, `[${id}](https://l24u.ru/topic/${id.replace('#', '')})`);
            });
        }

        if (!message || message === ''){
            message = '8 символов';
        }

        let body = { cid: cId.toString(), title: title, content:message };
        let formData = querystring.stringify(body);
        let contentLength = formData.length;
        let options = _.merge({
            url: Config.urlToBbTopicApi,
            headers: {
                    'Content-Type': 'application/x-www-form-urlencoded',
                    'Content-Length': contentLength
            },
            body: formData,
            method:'POST'
        }, this.options)
        console.log(options);
        request(
            options,
            function (error, response, body) {
                if (!error && response.statusCode == 200) {
                    //console.log(JSON.stringify(response.headers["set-cookie"]));
                    console.log('ok post ' + body);;
                    let b = JSON.parse(body);
                    let tid = b.payload.topicData.tid;
                    console.log(tid);
                    def.resolve({cookie:response.headers["set-cookie"], tid:b.payload.topicData.tid});
                } else {
                    console.log(error);
                    console.log(body);
                    console.log(response.statusCode);
                    def.reject();
                }
            }
        );
        return def.promise;
        
    }

    public closeTopics = (message:string, opts) => {
        let def = q.defer();
        let re = /\S*#(?:\[[^\]]+\]|\S+)/;
        console.log('in closeTopics, message arg - ' + message + ' ' + JSON.stringify(opts));
        let topicsIds = message.match(re);

        console.log(JSON.stringify(topicsIds));
        
        if (topicsIds && topicsIds.length > 0){
            topicsIds.forEach((id)=> {
                
                let url = Config.urlToBbTopicReadApi + id.replace('#','');
                let cook = opts.cookie[0];
                console.log(`in foreach ${id} ${url} ${cook}`);
                 let options = _.merge({
                    url: url,
                    method:'GET',
                    headers:{
                        Cookie:cook
                    }
                }, this.options);
                console.log('f');
                this.getTopicTitleAndPostId(options).then((data)=>{
                    this.modifyTopic(data, options, id).then(()=>{
                        this.postLink(opts.tid, id).then(()=>{
                            this.addTag(id);
                        });
                    }).catch(()=>{
                        def.reject();
                    });
                });
                
            });
        } else {
            def.reject();
        }
       return def.promise;
    }

    public addTag = (id) => {
        let def = q.defer();
        console.log('in addTag, message arg -  ' + id);
        let result:any[] = [];
        if (this.tags && this.tags.length && this.tags.length > 0){
            console.log(`got some tags - ${this.tags.length}`);
            this.tags.forEach((obj)=>{
                result.push(obj.value);
            });
        };
        result.push("CLOSED");
        let body = { "tags[]": result};
        let formData = querystring.stringify(body);
        let contentLength = formData.length;
        
        let options = _.merge({
            url:Config.urlToBbTopicApi + id.replace('#', '') + '/tags',
            headers: {
                    'Content-Type': 'application/x-www-form-urlencoded',
                    'Content-Length': contentLength
            },
            body: formData,
            method:'POST'
        }, this.options);

       request(
            options,
            function (error, response, body) {
                if (!error && response.statusCode == 200) {
                    console.log('ok addTag ' + JSON.stringify(body));
                    def.resolve('finish - ' +body);
                } else {
                    console.log(error);
                    console.log(body);
                    console.log(response.statusCode);
                    def.reject(`${error} ${body}`);
                }
            }
        );

       return def.promise;
    }

     public postLink = (tid, id) => {
        let def = q.defer();
        console.log('in postLink, message arg - ' + tid + ' ' + id);
        let body = { content: 'http://l24u.ru/topic/' + tid};
        let formData = querystring.stringify(body);
        let contentLength = formData.length;
        
        let options = _.merge({
            url:Config.urlToBbTopicApi + id.replace('#', ''),
            headers: {
                    'Content-Type': 'application/x-www-form-urlencoded',
                    'Content-Length': contentLength
            },
            body: formData,
            method:'POST'
        }, this.options);

       request(
            options,
            function (error, response, body) {
                if (!error && response.statusCode == 200) {
                    console.log('ok postLink ' + JSON.stringify(body));
                    def.resolve('finish - ' +body);
                } else {
                    console.log(error);
                    console.log(body);
                    console.log(response.statusCode);
                    def.reject(`${error} ${body}`);
                }
            }
        );

       return def.promise;
    }

    modifyTopic = (data, options, id) => {
        console.log('in modifytopic');
        let def:Q.Deferred<string> = q.defer();

        let message = entities.decode(data.content);
        console.log(message);

        message = md(message);

        console.log('MARKDOWN ' + message);

        let body = { pid: data.pid, title: '[CLOSED] ' + data.title, content: message};
        let formData = querystring.stringify(body);
        let contentLength = formData.length;
        options = _.merge({
            url:Config.urlToBbTopicApi + id.replace('#', ''),
            headers: {
                    'Content-Type': 'application/x-www-form-urlencoded',
                    'Content-Length': contentLength
            },
            body: formData,
            method:'PUT'
        }, this.options)

        
        request(
            options,
            function (error, response, body) {
                if (!error && response.statusCode == 200) {
                    console.log('ok modifyTopic' + JSON.stringify(body));
                    def.resolve('finish - ' +body);
                } else {
                    console.log(error);
                    console.log(body);
                    console.log(response.statusCode);
                    def.reject(`${error} ${body}`);
                }
            }
        );
        return def.promise;

    }

    getTopicTitleAndPostId = (options) => {
        console.log('in getTopicTitleAndPostId, message arg - ' + JSON.stringify(options));
        let def:Q.Deferred<any> = q.defer();
        //console.log(JSON.stringify(options));
        //if (cookie && cookie.length > 0){
            // console.log(cookie[0]);
            // var j = request.jar();
            // let cook = request.cookie(cookie[0]);
            // var url = 'http://l24u.ru';
            // j.setCookie(cook, url);
            // console.log(j.getCookie(url));
            request(
                options,
                 (error, response, body) => {
                    if (!error && response.statusCode == 200) {
                        //console.log('ok ' + body);
                        body = JSON.parse(body);
                        let postId = body.posts[0].pid;
                        let content = body.posts[0].content;
                        let result = {title:body.title, pid:postId, content:content};
                        console.log('ok getTopicTitleAndPostId' + JSON.stringify(result));
                        this.tags = body.tags;
                        def.resolve(result);
                    } else {
                        console.log(error);
                        console.log(body);
                        console.log(response.statusCode);
                        def.reject(`${error} ${body}`);
                    }
                }
            );
        //}
        
        return def.promise;

    }
}